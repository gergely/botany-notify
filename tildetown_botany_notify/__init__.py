"""Check your tilde.town botany garden
"""

from datetime import datetime
import json
import sys

import notify2
from paramiko.client import SSHClient
from paramiko.ssh_exception import SSHException


def get_botany_json(username):
    ssh_client = SSHClient()
    ssh_client.load_system_host_keys()
    ssh_client.connect('tilde.town', username=username)

    with ssh_client.open_sftp() as sftp_client:
        with sftp_client.open(f'.botany/{username}_plant_data.json') as remote_file:
            botany_json_data = remote_file.read()

    return json.loads(botany_json_data)


def get_water_level(botany_json):
    timestamp = botany_json['last_watered']
    last_water_time = datetime.utcfromtimestamp(timestamp)
    now = datetime.utcnow()
    not_watered_for = (now - last_water_time).total_seconds()

    return 100 - (not_watered_for / 86400) * 100


def show_notification(title, message, message_type=notify2.URGENCY_NORMAL, icon='task-due'):
    notify2.init('town.tilde.botany')
    n = notify2.Notification(title, message, icon)
    n.set_urgency(message_type)
    n.set_timeout(10000)
    n.show()


def main():
    username = sys.argv[1]

    if len(sys.argv) > 2:
        threshold = float(sys.argv[2])
    else:
        threshold = 40

    try:
        botany_json = get_botany_json(username)
    except SSHException:
        show_error('Botany error', 'Could not connect to tilde.town')

        return
    except json.JSONDecodeError:
        show_error('Botany error', 'Invalid botany data fetched from tilde.town')

        return

    try:
        water_level = get_water_level(botany_json)
    except KeyError:
        show_error('Botany error', 'Watering time missing from botany data')

        return

    if water_level < threshold:
        show_notification('Your plant is drying',
                          f'Your plant on tilde.town has a water level of {water_level:.2f}')

main()
